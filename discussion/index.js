db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "8785852",
        email: "janedoe@gmail.com",
    },
    courses: ["CSS", "Javascript", "Python"],
    department: "none",
});

db.users.insertMany([
    {
        firstName: "Jane1",
        lastName: "Doe1",
        age: 211,
        contact: {
            phone: "87858521",
            email: "janedoe@gmail.com1",
        },
        courses: ["CSS1", "Javascript1", "Python1"],
        department: "none1",
    },
    {
        firstName: "Jane11",
        lastName: "Doe11",
        age: 2111,
        contact: {
            phone: "878585211",
            email: "janedoe@gmail.com11",
        },
        courses: ["CSS11", "Javascrip1t1", "Pyth1on1"],
        department: "none11",
    },
]);

db.users.find();

db.users.updateOne(
    { firstName: "Jane1" },
    {
        $set: {
            firstName: "Jane1222",
            lastName: "Doe122",
            age: 211,
            contact: {
                phone: "8785852122",
                email: "janedoe@gmail.com122",
            },
            courses: ["CSS1", "Javascript1", "Python1"],
            department: "non22e1",
        },
    }
);
